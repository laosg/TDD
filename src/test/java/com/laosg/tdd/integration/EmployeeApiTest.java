package com.laosg.tdd.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laosg.tdd.TDDRun;
import com.laosg.tdd.model.Employee;
import com.laosg.tdd.repository.EmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = TDDRun.class
)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class EmployeeApiTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeRepository employeeRepository;
    @Test
    public void giveEmployees_whenGetEmployess_thenStatus200() throws Exception {
        List<Employee> employees=new ArrayList();
        employees.add(new Employee(1,"kevin","kevin@163.com"));
        employees.add(new Employee(2,"kavin","kavin@163.com"));
        employeeRepository.saveAll(employees);
        mockMvc.perform(
                get("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name",is("kevin")))
                .andExpect(jsonPath("$",hasSize(2)));

    }


    @Test
    public void whenPostEmployess_thenStatus200() throws Exception {
        Employee employee = new Employee(5, "kevin2", "kevin@163.com");
        String json = new ObjectMapper().writeValueAsString(employee);
        mockMvc.perform(
                post("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON).content(json)
        ).andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name",is("kevin2")))
                .andExpect(jsonPath("$.id",is(notNullValue())));

        assertThat(employeeRepository.findAll().size()).isEqualTo(1);

    }
}
