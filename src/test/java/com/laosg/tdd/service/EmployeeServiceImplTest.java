package com.laosg.tdd.service;


import com.laosg.tdd.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
public class EmployeeServiceImplTest {

    private EmployeeService employeeService=new EmployeeServiceImpl();

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_given_employee() {
        Employee kk = new Employee(4, "kk", "123");
        employeeService.createEmployee(kk);
    }

    @Test
    public void createEmployee() {
    }
}
