package com.laosg.tdd.service;

import com.laosg.tdd.model.Employee;

import java.util.List;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
public interface EmployeeService {
    List<Employee> getAllEmplyees();

    Employee createEmployee(Employee employee);


}
