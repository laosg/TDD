package com.laosg.tdd.service;

import com.laosg.tdd.model.Employee;
import com.laosg.tdd.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmplyees() {
//        List<Employee> employees=new ArrayList();
//        employees.add(new Employee(1,"kevin","kevin@163.com", LocalDateTime.now()));
//        employees.add(new Employee(2,"kavin","kavin@163.com", LocalDateTime.now()));
//
//        return employees;
        return employeeRepository.findAll();
    }

    @Override
    public Employee createEmployee(Employee employee) {
        if (employee.getEmail().equals("123")) {
            throw new IllegalArgumentException();
        }
        return employeeRepository.save(employee);
    }

}
