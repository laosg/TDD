package com.laosg.tdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
@SpringBootApplication
public class TDDRun {

    public static void main(String[] args) {
        SpringApplication.run(TDDRun.class,args);
    }
}
