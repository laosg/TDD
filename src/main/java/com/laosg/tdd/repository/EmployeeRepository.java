package com.laosg.tdd.repository;

import com.laosg.tdd.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

}
