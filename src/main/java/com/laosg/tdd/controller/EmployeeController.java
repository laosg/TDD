package com.laosg.tdd.controller;

import com.laosg.tdd.model.Employee;
import com.laosg.tdd.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>description: some thing </p>
 *
 * @author kevin
 * @date 2020/8/20
 */
@RestController
@RequestMapping("/api/employees")
@AllArgsConstructor
public class EmployeeController {


    private final EmployeeService employeeSerivce;

    @GetMapping
    public List<Employee> employees() {
        return employeeSerivce.getAllEmplyees();
    }

    @PostMapping
    public Employee create(@RequestBody Employee employee) {
        return employeeSerivce.createEmployee(employee);
    }
}
